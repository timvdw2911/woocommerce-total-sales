<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       aldofieuw.com
 * @since      1.0.0
 *
 * @package    Woototalvariations
 * @subpackage Woototalvariations/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->

<?php
/**
 * @snippet       Show Total Sales @ WooCommerce Single Product
 * @how-to        Get CustomizeWoo.com FREE
 * @author        Rodolfo Melogli
 * @compatible    Woo 4.5
 * @donate $9     https://businessbloomer.com/bloomer-armada/
 */

add_action('woocommerce_single_product_summary', 'bbloomer_product_sold_count', 11);

function bbloomer_product_sold_count()
{
    global $product;
    print_r($product);
    // $units_sold = $product->get_total_sales();
    // if ($units_sold) echo '<p>' . sprintf(__('Units Sold: %s', 'woocommerce'), $units_sold) . '</p>';
}
